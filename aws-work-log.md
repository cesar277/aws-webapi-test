# **AWS Practice Test Work log**

## Overview

 Feel free to add everything you consider in order to understand why you implement those approaches and what are the benefits them in terms of:

1. Operational
1. Security
1. Reliability
1. Performance
1. Cost optimization

### Part 01 Explain all steps of Deploy serverless architecture

#### 01 - Steps to achive goals

##### DynamoDb Database  

Two tables have been created, 'Flight' and 'Reservation'. The billing method chosen was on-demand due to its advantage of auto scale requests.

![Create Table](aws-resources/imgs/AmazonDynamoCreateTAble.PNG)

To create the base date, the client can be used with the scripts available in the date folder or the use of the web tool aws DynamoDB connected to the user's profile

![New Table](aws-resources/imgs/DynamoNewTable.PNG)

##### Lambda

LambdaTool
a dynamoDb context was created together with the Database Models configured with PropertyConverter to map the data.

![Lambda Tolls](aws-resources/imgs/LambdaTool.PNG)

The deployment can be done directly by aws CLI or uploud in the aws tool.

dotnet lambda deploy-function

![Lambda Diagram](aws-resources/imgs/LambdaDiagram.PNG)

#### 01 - Beneficts of you solution

Thinking about the good practices of the lambdas functions together with the focus of the functionality which is a virtualization of functions on demand the cost coming from the execution speed and the demand of them was made to be as fast as possible with specific data structures and configured for return. The cost benefit of a database on demand for a feature that can have N accesses due to customer demand makes having a specific cost the need per month.

The return based on https logs makes it easy to understand what happens with the api. And with the api tests it is easy to apply TDD.

#### 01 - Notes

Two solutions to this problem were created, one applying the diagram solution with a focus on lambda functions and the other a serveless funcition with the same code structure but with a difference. It was chosen to present the application with more similarity with the diagram given for the test

The advantages of the serverless application along with accepting multiple functions and more advantages for configurations.


##### serveless

Serveless was created with the focus of being connected directly with the api gateway and not to the webservice, it was thinking about this that the structure receives apigateway APIGatewayProxyResponse which is an alternative to making the webservice connect with the call.
![serveless Diagram](aws-resources/imgs/serveless.PNG)



### Part 03 Explain all steps of Deploy Ryanair API on EC2 instances

#### 03 - Steps to achive goals

##### EC2
First select the EC2 service.

![Select EC2](aws-resources/imgs/step3_1.png)

Select lauch instance.

![Lauch instance](aws-resources/imgs/step3_2.png)

Select a machine image.

![Select machine instance](aws-resources/imgs/step3_3.png)

Select a machine type.

![Select machine type](aws-resources/imgs/step3_4.png)

Confirm your choices.

![Confirm choices](aws-resources/imgs/step3_5.png)

Select a key pair that allow you to connect to your new instance.

![Select keys](aws-resources/imgs/step3_6.png)

Check your new instance.

![Check instance](aws-resources/imgs/step3_7.png)

Now it is necessary to upload the application to the EC2 server.

##### API Gateway

First select the API Gateway service.

![Check instance](aws-resources/imgs/step3_8.png)

Select build Rest API

![Check instance](aws-resources/imgs/step3_9.png)

Choose 'Import from Swagger or Open API 3' and write your swagger.json file

![Check instance](aws-resources/imgs/step3_10.png)

Now that all API endpoints are created, all that's left is to configure the endpoint of the EC2 machine created earlier

![Check instance](aws-resources/imgs/step3_11.png)


![Check instance](aws-resources/imgs/gatewayApi.PNG)

#### 03 - Beneficts of you solution

*write here*

#### 03 - Link your architecture diagram of your solution

![Lambda Diagram](aws-resources/imgs/LambdaDiagram.PNG)

#### 03 -Notes

Some of the tools used to develop the solution were:
* AutoMapper (to map entities)
* Async / Await calls
* Exception middleware
* Logger
* HealthChecks
