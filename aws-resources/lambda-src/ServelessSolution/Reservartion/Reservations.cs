using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Amazon.Lambda.Core;
using Amazon.Lambda.APIGatewayEvents;

using Amazon;

using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.Model;
using System.Net;
using Newtonsoft.Json;
using Amazon.Runtime.CredentialManagement;
using dotnetlambda.domain;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace dotnetlambda
{
    public class Reservations
    {



        private readonly AmazonDynamoDBClient _amazonDynamoDBClient;
        private readonly DynamoDBContext _context;

        public Reservations()
        {

            //localstack ignores secrets
            _amazonDynamoDBClient = new AmazonDynamoDBClient("Acess", "Key",
                new AmazonDynamoDBConfig
                {
                    UseHttp = true,

                    ServiceURL = "http://localhost:8000"
                });

            _context = new DynamoDBContext(_amazonDynamoDBClient, new DynamoDBContextConfig { });


        }

        /// <summary>
        ///  A Lambda function that returns all Reservartion
        /// </summary>
        /// <returns>List of Flights</returns>
        public async Task<APIGatewayProxyResponse> GetReservartionAsync(APIGatewayProxyRequest request, ILambdaContext context)
        {
            context.Logger.LogLine("Getting Reservartion");
            var conditions = new List<ScanCondition>();
            var search = await _context.ScanAsync<Reservations>(conditions).GetNextSetAsync();
            context.Logger.LogLine($"Found Reservartion");
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(search),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };
            return response;
        }
        /// <summary>
        /// A Lambda function that adds a Reservation post.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<APIGatewayProxyResponse> AddReservartionAsync(APIGatewayProxyRequest request, ILambdaContext context)
        {
            var Reservations = JsonConvert.DeserializeObject<Reservation>(request?.Body);
            Reservations.Id = Guid.NewGuid().GetHashCode();
            //Reservations.CreatedTimestamp = DateTime.Now;

            context.Logger.LogLine($"Saving Reservations with id {Reservations.Id}");
            await _context.SaveAsync<Reservation>(Reservations);

            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = Reservations.Id.ToString(),
                Headers = new Dictionary<string, string> { { "Content-Type", "text/plain" } }
            };
            return response;
        }




    }
}