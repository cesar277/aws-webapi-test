﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;

namespace dotnetlambda.domain
{
    [DynamoDBTable("Flight")]
    public class Flight
    {
        [DynamoDBHashKey]
        public string Key { get; set; }

        [DynamoDBProperty]
        public string Origin { get; set; }

        [DynamoDBProperty]
        public string Destination { get; set; }

        [DynamoDBProperty]
        public DateTime Time { get; set; }
    }

    public class FlightConverter : IPropertyConverter
    {
        DynamoDBEntry IPropertyConverter.ToEntry(object value)
        {
            List<Flight> flights = value as List<Flight>;
            if (flights == null) return null;
            string jsonString = JsonSerializer.Serialize(flights);
            DynamoDBEntry entry = new Primitive
            {
                Value = jsonString
            };
            return entry;
        }

        public object FromEntry(DynamoDBEntry entry)
        {
            Primitive primitive = entry as Primitive;
            if (primitive == null || !(primitive.Value is String) || string.IsNullOrEmpty((string)primitive.Value))
                return null;
            List<Flight> flights = JsonSerializer.Deserialize<List<Flight>>((string)primitive.Value);
            return flights;
        }
    }

}





