using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Amazon.Lambda.Core;
using Amazon.Lambda.APIGatewayEvents;

using Amazon;

using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.Model;
using System.Net;
using Newtonsoft.Json;
using Amazon.Runtime.CredentialManagement;
using dotnetlambda.domain;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace dotnetlambda
{
    public class Flights
    {
        private readonly AmazonDynamoDBClient _amazonDynamoDBClient;
        private readonly DynamoDBContext _context;
        public Flights()
        {

         
             _amazonDynamoDBClient = new AmazonDynamoDBClient("Acess", "Key",
                 new AmazonDynamoDBConfig
                 {
                     UseHttp = true,

                     ServiceURL = "http://localhost:8000"
                 });

            _context = new DynamoDBContext(_amazonDynamoDBClient, new DynamoDBContextConfig{});


        }

        /// <summary>
        ///  A Lambda function that returns all Flights
        /// </summary>
        /// <returns>List of Flights</returns>
        public async Task<APIGatewayProxyResponse> GetFlightsAsync(APIGatewayProxyRequest request, ILambdaContext context)
        {
            context.Logger.LogLine("Getting Flights");
            var conditions = new List<ScanCondition>();   
            var search = await _context.ScanAsync<Flight>(conditions).GetNextSetAsync();
            context.Logger.LogLine($"Found Flights");
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(search),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;
        }




    }
}