﻿using System.Collections.Generic;
using System.Linq;
using Amazon.DynamoDBv2.DataModel;


namespace dotnetlambda.domain
{
    [DynamoDBTable("Reservation")]
    public class Reservation
    {
        [DynamoDBHashKey]
        public int Id { get;  set; }

        [DynamoDBProperty]
        public string ReservationNumber { get;  set; }

        [DynamoDBProperty]
        public string Email { get;  set; }

        [DynamoDBProperty]
        public string CreditCard { get;  set; }

        [DynamoDBProperty(typeof(FlightConverter))]
        public Flight InboundFlight { get;  set; }

        [DynamoDBProperty(typeof(FlightConverter))]
        public Flight OutboundFlight { get;  set; }

        [DynamoDBProperty(typeof(PassagersConverter))]
        public List<Passenger> PassengersInboundFlight { get;  set; }

        [DynamoDBProperty(typeof(PassagersConverter))]
        public List<Passenger> PassengersOutboundFlight { get;  set; }
        
        [DynamoDBIgnore]
        public bool IsRoundTrip => this.InboundFlight != null;
    }
}
