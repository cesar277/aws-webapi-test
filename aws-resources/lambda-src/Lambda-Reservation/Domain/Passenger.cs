﻿using System.Collections.Generic;
using System.Text.Json;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;

namespace dotnetlambda.domain
{
    public class Passenger
    {
        public string Name { get; }

        public int Bags { get; }

        public string Seat { get; }
    }

    public class PassagersConverter : IPropertyConverter
    {
        DynamoDBEntry IPropertyConverter.ToEntry(object value)
        {
            List<Passenger> passengers = value as List<Passenger>;
            if (passengers == null) return null;
            string jsonString = JsonSerializer.Serialize(passengers);
            DynamoDBEntry entry = new Primitive
            {
                Value = jsonString
            };
            return entry;
        }

        public object FromEntry(DynamoDBEntry entry)
        {
            Primitive primitive = entry as Primitive;
            if (primitive == null || !(primitive.Value is string) || string.IsNullOrEmpty((string)primitive.Value))
                return null;
            List<Passenger> passengers = JsonSerializer.Deserialize<List<Passenger>>((string)primitive.Value);
            return passengers;
        }
    }
}
