using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Amazon.Lambda.Core;
using Amazon.Lambda.APIGatewayEvents;

using Amazon;

using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.Model;
using System.Net;
using Newtonsoft.Json;
using Amazon.Runtime.CredentialManagement;
using dotnetlambda.domain;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]


namespace dotnetlambda
{
    public class Reservations
    {



        private readonly AmazonDynamoDBClient _amazonDynamoDBClient;
        private readonly DynamoDBContext _context;

        public Reservations()
        {

            //localstack ignores secrets

            _amazonDynamoDBClient = new AmazonDynamoDBClient();
            _context = new DynamoDBContext(_amazonDynamoDBClient, new DynamoDBContextConfig { });



        }



        /// <summary>
        ///  A Lambda function that returns all Reservation
        /// </summary>
        /// <returns>List of Flights</returns>
        public async Task<string> HandlerReservartion(string request, ILambdaContext context)
        {

            if (string.IsNullOrEmpty(request))
                return await GetReservartionAsync(request, context);
            else
                return await AddReservartionAsync(request, context);

            return HttpStatusCode.NotFound.ToString();


        }



        private async Task<string> GetReservartionAsync(string request, ILambdaContext context)
        {
            context.Logger.LogLine("Getting Reservation");
            var conditions = new List<ScanCondition>();
            var search = await _context.ScanAsync<Reservations>(conditions).GetNextSetAsync();
            context.Logger.LogLine($"Found Reservartion");

            return JsonConvert.SerializeObject(search);
        }


        public async Task<string> AddReservartionAsync(string request, ILambdaContext context)
        {
            var Reservations = JsonConvert.DeserializeObject<Reservation>(request);
            Reservations.Id = Guid.NewGuid().GetHashCode();
            Reservations.ReservationNumber = Reservations.Id.ToString();

            //Reservations.CreatedTimestamp = DateTime.Now;

            context.Logger.LogLine($"Saving Reservations with id {Reservations.Id}");
            await _context.SaveAsync<Reservation>(Reservations);


            return Reservations.ReservationNumber.ToString();
        }




    }
}