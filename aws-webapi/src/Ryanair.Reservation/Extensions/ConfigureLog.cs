using Microsoft.Extensions.Configuration;
using Serilog;

namespace Ryanair.Reservation.Extensions {
    public static class ConfigureLog {
        public static void ConfigureLogging(IConfiguration config, string environmentName) {

            var logConfig = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .ReadFrom.Configuration(config)
                .WriteTo.Debug()
                .WriteTo.Console()
                .Enrich.WithProperty("Environment", environmentName);

            Log.Logger = logConfig.CreateLogger();
        }

    }
}