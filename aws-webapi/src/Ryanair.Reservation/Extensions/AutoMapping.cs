using AutoMapper;

namespace Ryanair.Reservation.Extensions {
    public class AutoMapping : Profile {
        public AutoMapping() {
            CreateMap<Models.Flight, Domain.Flight>();
            CreateMap<Domain.Flight, Models.Flight>();

            CreateMap<Models.Passenger, Domain.Passenger>();
            CreateMap<Domain.Passenger, Models.Passenger>();

            CreateMap<Domain.Reservation, Models.ReservationFlight>()
                    .ForMember(dest => dest.Key, opt => opt.MapFrom(src => src.ReservationNumber))
                    .ForMember(dest => dest.Passengers, opt => opt.MapFrom(src => src.ReservationNumber));
        }
    }
}