﻿﻿using Microsoft.AspNetCore.Mvc;
using Ryanair.Reservation.Models;
using System.Collections.Generic;
using Ryanair.Reservation.ApplicationService.Interfaces;
using System.Threading.Tasks;
using AutoMapper;

namespace Ryanair.Reservation.Controllers {
    [Route("[controller]")]
    [ApiController]
    public class FlightController : ControllerBase {
        private readonly IMapper _mapper;

        private readonly IFlightQueryService _flightQueryService;

        public FlightController(IMapper mapper, IFlightQueryService flightQueryService) {
            _mapper = mapper;
            _flightQueryService = flightQueryService;
        }

        [HttpGet]
        public async Task<ActionResult<List<Flight>>> Get([FromQuery] GetFlights getFlights) {
            var flights = await _flightQueryService.GetAsync(getFlights.Passengers, getFlights.Origin, getFlights.Destination, getFlights.DateOut, getFlights.DateIn, getFlights.RoundTrip);
            var modelFlights = _mapper.Map<List<Flight>>(flights);
            return modelFlights;
        }
    }
}
