﻿﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Ryanair.Reservation.ApplicationService.Interfaces;
using Ryanair.Reservation.Models;

namespace Ryanair.Reservation.Controllers {
    [Route("[controller]")]
    [ApiController]
    public class ReservationController : ControllerBase {
        private readonly IMapper _mapper;
        private readonly IReservationService _service;
        private readonly IReservationQueryService _queryService;

        public ReservationController(IMapper mapper, IReservationService service, IReservationQueryService queryService) {
            _mapper = mapper;
            _service = service;
            _queryService = queryService;
        }

        [HttpGet("{key}")]
        public async Task<ActionResult<Models.Reservation>> Get(string key) {
            var reservation = await _queryService.GetByReservationKeyAsync(key);

            if (reservation == null) {
                return NotFound();
            }

            List<ReservationFlight> flights = new List<ReservationFlight>();
            List<Models.Passenger> passengersInbound = _mapper.Map<List<Models.Passenger>>(reservation.PassengersInboundFlight);
            flights.Add(new ReservationFlight() {
                Key = reservation.InboundFlight.Key,
                Passengers = passengersInbound
            });
            if (reservation.IsRoundTrip) {
                List<Models.Passenger> passengersOutbound = _mapper.Map<List<Models.Passenger>>(reservation.PassengersOutboundFlight);
                flights.Add(new ReservationFlight() {
                    Key = reservation.OutboundFlight.Key,
                    Passengers = passengersOutbound
                });
            }
            return new Models.Reservation {
                ReservationNumber = reservation.ReservationNumber,
                Email = reservation.Email,
                Flights = flights
            };
        }

        [HttpPost]
        public async Task<ActionResult> Post(NewReservationRequest newReservationRequest) {
            List<Domain.Passenger> outboundPassengers =
                    _mapper.Map<List<Domain.Passenger>>(newReservationRequest.GetOutBoundFlight().Passengers);
            Domain.Reservation result;

            if (newReservationRequest.IsRoundTrip) {
                List<Domain.Passenger> inboundPassengers =
                        _mapper.Map<List<Domain.Passenger>>(newReservationRequest.GetInBoundFlight().Passengers);

                result = await _service.BookRoundTrip(newReservationRequest.Email, newReservationRequest.CreditCard, newReservationRequest.GetOutBoundFlight().Key, outboundPassengers, newReservationRequest.GetInBoundFlight().Key, inboundPassengers);
            } else {
                result = await _service.BookOneWayTrip(newReservationRequest.Email, newReservationRequest.CreditCard, newReservationRequest.GetOutBoundFlight().Key, outboundPassengers);
            }

            if (result == null) {
                return BadRequest();
            }

            return Ok(result.ReservationNumber);
        }
    }
}
