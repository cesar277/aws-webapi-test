﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Ryanair.Reservation.ApplicationService;
using Ryanair.Reservation.ApplicationService.Interfaces;
using Ryanair.Reservation.Repositories;
using Ryanair.Reservation.Middlewares;
using Amazon.Lambda;
using Ryanair.Reservation.Extensions;

namespace Ryanair.Reservation {
    public class Startup {
        private IConfiguration _configuration { get; }
        public Startup(IConfiguration configuration, IWebHostEnvironment env) {
            _configuration = configuration;
            ConfigureLog.ConfigureLogging(_configuration, env.EnvironmentName);
        }

        public void ConfigureServices(IServiceCollection services) {
            this.ConfigureRepositories(services);
            this.ConfigureApplicationServices(services);

            services.AddDefaultAWSOptions(_configuration.GetAWSOptions());
            services.AddAutoMapper(typeof(Startup));

            services.AddHealthChecks();
            services.AddSwaggerGen();

            services.AddMvcCore().AddApiExplorer();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<ExceptionMiddleware>();

            app.UseSwagger();
            app.UseSwaggerUI(c => {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "BlueETL Reservations API");
            });

            app.UseRouting();

            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
                endpoints.MapHealthChecks("/health");
            });

        }

        private void ConfigureRepositories(IServiceCollection services) {
            services.AddSingleton<IFlightRepository, FlightRepository>();
            services.AddSingleton<IReservationRepository, ReservationRepository>();
        }

        private void ConfigureApplicationServices(IServiceCollection services) {
            services.AddScoped<IFlightQueryService, FlightQueryService>();
            services.AddScoped<IReservationService, ReservationService>();
            services.AddScoped<IReservationQueryService, ReservationQueryService>();
        }
    }
}
