﻿using System.Collections.Generic;

namespace Ryanair.Reservation.Models {
    public class ReservationFlight {
        public string Key { get; set; }

        public List<Passenger> Passengers { get; set; }
    }
}
