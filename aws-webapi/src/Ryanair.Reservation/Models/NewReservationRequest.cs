﻿using System.Collections.Generic;

namespace Ryanair.Reservation.Models {
    public class NewReservationRequest {
        public string Email { get; set; }

        public string CreditCard { get; set; }

        public List<ReservationFlight> Flights { get; set; }

        public bool IsRoundTrip => this.Flights?.Count == 2;

        public ReservationFlight GetOutBoundFlight() => this.Flights[0];

        public ReservationFlight GetInBoundFlight() => this.IsRoundTrip ? this.Flights[1] : null;
    }
}
