using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Ryanair.Reservation.ApplicationService.Exceptions;

namespace Ryanair.Reservation.Middlewares {
    public class ExceptionMiddleware {
        private readonly RequestDelegate _next;
        private readonly ILogger<ExceptionMiddleware> _logger;

        public ExceptionMiddleware(RequestDelegate next, ILogger<ExceptionMiddleware> logger) {
            _next = next;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext httpContext) {
            try {
                await _next(httpContext);
            } catch (Exception ex) {
                await HandleExceptionAsync(httpContext, ex);
            }
        }

        private async Task HandleExceptionAsync(HttpContext context, Exception exception) {
            context.Response.ContentType = "application/json";
            if (exception.GetType() == typeof(BusinessException)) {
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                string message = exception.Message;
                _logger.LogInformation(exception, "An business logic error occurred: {message}", message);
                await context.Response.WriteAsync(message);
            } else {
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                var info = new {
                    context.Response.StatusCode,
                    message = "Internal Server Error from the ExceptionMiddleware.",
                    exception = exception.Message
                };
                _logger.LogError(exception, "An unknown error occurred: {info}", info);
                await context.Response.WriteAsync(info.ToString());
            }
        }
    }
}