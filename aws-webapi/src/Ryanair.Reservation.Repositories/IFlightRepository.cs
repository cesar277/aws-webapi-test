﻿using Ryanair.Reservation.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ryanair.Reservation.Repositories {
    public interface IFlightRepository {
        Task<List<Flight>> GetAsync();
    }
}
