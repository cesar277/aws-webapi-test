﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ryanair.Reservation.Repositories {
    public interface IReservationRepository {
        //Task<Domain.Reservation> GetByReservationKeyAsync(string reservationKey);
        Task<List<Domain.Reservation>> GetAsync();
        Task<Domain.Reservation> PostAsync(Domain.Reservation reservation);
    }
}
