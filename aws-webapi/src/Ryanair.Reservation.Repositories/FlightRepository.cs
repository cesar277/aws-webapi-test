﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;
using Amazon;
using Amazon.Lambda;
using Amazon.Lambda.Model;

namespace Ryanair.Reservation.Repositories {
    public class FlightRepository : IFlightRepository {
        private AmazonLambdaClient _lambda;
        public FlightRepository() {
            _lambda = new AmazonLambdaClient("AWSAccessKey", "AWSSecretKey", RegionEndpoint.USEast1);
        }
        public async Task<List<Domain.Flight>> GetAsync() {
            var lambdaRequest = new InvokeRequest {
                FunctionName = "GetFlightsAsync",
            };

            var response = await _lambda.InvokeAsync(lambdaRequest);
            if (response != null) {
                List<Domain.Flight> flights;
                using (var sr = new StreamReader(response.Payload)) {
                    string flightStr = await sr.ReadToEndAsync();
                    flights = JsonSerializer.Deserialize<List<Domain.Flight>>(flightStr);
                }
                return flights;
            }
            return null;
        }
    }
}