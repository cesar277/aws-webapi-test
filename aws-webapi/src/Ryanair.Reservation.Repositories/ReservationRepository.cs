﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Amazon;
using Amazon.Lambda;
using Amazon.Lambda.Model;
using System.Text.Json;

namespace Ryanair.Reservation.Repositories {
    public class ReservationRepository : IReservationRepository {
        private AmazonLambdaClient _lambda;
        public ReservationRepository() {
            _lambda = new AmazonLambdaClient("AWSAccessKey", "AWSSecretKey", RegionEndpoint.USEast1);
        }

        public async Task<List<Domain.Reservation>> GetAsync() {
            var lambdaRequest = new InvokeRequest {
                FunctionName = "HandlerReservation",
            };

            var response = await _lambda.InvokeAsync(lambdaRequest);
            if (response != null) {
                List<Domain.Reservation> reservations;
                using (var sr = new StreamReader(response.Payload)) {
                    string reservationsStr = await sr.ReadToEndAsync();
                    reservations = JsonSerializer.Deserialize<List<Domain.Reservation>>(reservationsStr);
                }
                return reservations;
            }
            return null;
        }

        public async Task<Domain.Reservation> PostAsync(Domain.Reservation reservation) {
            var lambdaRequest = new InvokeRequest {
                FunctionName = "HandlerReservation",
                Payload = JsonSerializer.Serialize(reservation)
            };

            var response = await _lambda.InvokeAsync(lambdaRequest);
            if (response != null) {
                using (var sr = new StreamReader(response.Payload)) {
                    string id = await sr.ReadToEndAsync();
                    reservation.Id = int.Parse(id);
                    reservation.ReservationNumber = id;
                }
                return reservation;
            }
            return null;
        }
    }
}