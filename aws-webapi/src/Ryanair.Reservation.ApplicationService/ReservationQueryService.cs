﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Ryanair.Reservation.ApplicationService.Interfaces;
using Ryanair.Reservation.Repositories;

namespace Ryanair.Reservation.ApplicationService {
    public class ReservationQueryService : IReservationQueryService {
        private readonly IReservationRepository _reservationRepository;

        public ReservationQueryService(IReservationRepository reservationRepository) {
            this._reservationRepository = reservationRepository;
        }

        public async Task<Domain.Reservation> GetByReservationKeyAsync(string reservationKey) {
            List<Domain.Reservation> reservations = await this._reservationRepository.GetAsync();
            return reservations.Find(x => x.ReservationNumber == reservationKey);
        }
    }
}
