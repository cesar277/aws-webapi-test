﻿using Ryanair.Reservation.Domain;
using Ryanair.Reservation.Repositories;
using System.Collections.Generic;
using Ryanair.Reservation.ApplicationService.Interfaces;
using System.Threading.Tasks;
using System.Linq;
using Ryanair.Reservation.ApplicationService.Exceptions;

namespace Ryanair.Reservation.ApplicationService {
    //
    // Rename, reorder refactor is allowed. Do whatever you think is the best choice.
    //

    public class ReservationService : IReservationService {
        private readonly IFlightRepository _flightRepository;
        private readonly IReservationRepository _reservationRepository;
        private int FlightSeatsCapacity = 50;
        private int FlightBagsCapacity = 50;
        private int bagsPerPassengerLimit = 5;
        public ReservationService(IFlightRepository flightRepository, IReservationRepository reservationRepository) {
            _flightRepository = flightRepository;
            _reservationRepository = reservationRepository;
        }

        public async Task<Domain.Reservation> BookRoundTrip(string email, string creditCard, string outboundFlightKey, List<Passenger> outboundPassengers, string inboundFlightKey, List<Passenger> inboundPassengers) {
            List<Domain.Reservation> reservations = await _reservationRepository.GetAsync();
            List<Domain.Flight> flights = await _flightRepository.GetAsync();
            Domain.Flight inboundFlight = flights.Find(x => x.Key == inboundFlightKey);
            if (inboundFlight == null) {
                return null;
            }
            Domain.Flight outboundFlight = flights.Find(x => x.Key == outboundFlightKey);
            if (outboundFlight == null) {
                return null;
            }

            int inboundFlightBagCount = calcPassengersBags(inboundPassengers);
            int outboundFlightBagCount = calcPassengersBags(outboundPassengers);

            if (inboundFlightBagCount == -1 || outboundFlightBagCount == -1) {
                throw new BusinessException("Amount of bags on inbound flight exceeds limit");
            }

            int inboundFlightSeatCount = calcReservationSeats(reservations, inboundFlightKey);
            int outboundFlightSeatCount = calcReservationSeats(reservations, outboundFlightKey);

            if (inboundFlightSeatCount + inboundPassengers.Count > FlightSeatsCapacity
                || outboundFlightSeatCount + outboundPassengers.Count > FlightSeatsCapacity) {
                throw new BusinessException("number of passengers on inbound flight exceeds limit");
            }

            int reservationInboundBags = calcReservationBags(reservations, inboundFlightKey);
            int reservationOutboundBags = calcReservationBags(reservations, outboundFlightKey);

            if (inboundFlightBagCount + reservationInboundBags > FlightBagsCapacity
                || outboundFlightBagCount + reservationOutboundBags > FlightBagsCapacity) {
                throw new BusinessException("number of passengers on outbound flight exceeds limit");
            }

            Domain.Reservation res = new Domain.Reservation() {
                Email = email,
                CreditCard = creditCard,
                InboundFlight = inboundFlight,
                OutboundFlight = outboundFlight,
                PassengersInboundFlight = inboundPassengers,
                PassengersOutboundFlight = outboundPassengers,
            };
            res = await _reservationRepository.PostAsync(res);
            return res;
        }

        public async Task<Domain.Reservation> BookOneWayTrip(string email, string creditCard, string flightKey, List<Passenger> passengers) {
            List<Domain.Reservation> reservations = await _reservationRepository.GetAsync();
            List<Domain.Flight> flights = await _flightRepository.GetAsync();
            Domain.Flight flight = flights.Find(x => x.Key == flightKey);
            if (flight == null) {
                return null;
            }

            int flightBagCount = calcPassengersBags(passengers);

            if (flightBagCount == -1) {
                throw new BusinessException("Amount of bags on inbound flight exceeds limit");
            }

            int flightSeatCount = calcReservationSeats(reservations, flightKey);

            if (flightSeatCount + passengers.Count > FlightSeatsCapacity) {
                throw new BusinessException("number of passengers on inbound flight exceeds limit");
            }

            int reservationBags = calcReservationBags(reservations, flightKey);

            if (flightBagCount + reservationBags > FlightBagsCapacity) {
                throw new BusinessException("number of passengers on outbound flight exceeds limit");
            }

            Domain.Reservation res = new Domain.Reservation() {
                Email = email,
                CreditCard = creditCard,
                InboundFlight = flight,
                PassengersInboundFlight = passengers,
            };
            res = await _reservationRepository.PostAsync(res);
            return res;
        }

        private int calcPassengersBags(List<Passenger> passengers) {
            int numBags = 0;
            foreach (var passenger in passengers) {
                if (passenger.Bags > bagsPerPassengerLimit) {
                    return -1;
                }
                numBags += passenger.Bags;
            }
            return numBags;
        }

        private int calcReservationSeats(List<Domain.Reservation> reservations, string FlightKey) {
            int flightSeats = 0;
            foreach (var reservation in reservations) {
                if (reservation.InboundFlight.Key == FlightKey) {
                    flightSeats += reservation.PassengersInboundFlight.Count;
                }
                if (reservation.OutboundFlight.Key == FlightKey) {
                    flightSeats += reservation.PassengersOutboundFlight.Count;
                }
            }
            return flightSeats;
        }

        private int calcReservationBags(List<Domain.Reservation> reservations, string FlightKey) {
            int bags = 0;
            foreach (var reservation in reservations) {
                if (reservation.InboundFlight.Key == FlightKey) {
                    foreach (var passenger in reservation.PassengersInboundFlight) {
                        bags += passenger.Bags;
                    }
                }
                if (reservation.OutboundFlight.Key == FlightKey) {
                    foreach (var passenger in reservation.PassengersInboundFlight) {
                        bags += passenger.Bags;
                    }
                }
            }
            return bags;
        }
    }
}
