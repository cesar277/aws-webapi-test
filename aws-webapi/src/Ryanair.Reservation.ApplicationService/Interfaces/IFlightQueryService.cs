﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ryanair.Reservation.Domain;

namespace Ryanair.Reservation.ApplicationService.Interfaces {
    public interface IFlightQueryService {
        Task<List<Flight>> GetAsync(int passengers, string origin, string destination, DateTime dateOut, DateTime dateIn, bool roundTrip);
    }
}
