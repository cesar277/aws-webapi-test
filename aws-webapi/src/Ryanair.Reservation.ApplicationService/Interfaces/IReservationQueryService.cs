﻿using System.Threading.Tasks;

namespace Ryanair.Reservation.ApplicationService.Interfaces {
    public interface IReservationQueryService {
        Task<Domain.Reservation> GetByReservationKeyAsync(string reservationKey);
    }
}
