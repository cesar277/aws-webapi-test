﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Ryanair.Reservation.Domain;

namespace Ryanair.Reservation.ApplicationService.Interfaces {
    public interface IReservationService {
        Task<Domain.Reservation> BookOneWayTrip(string email, string creditCard, string flightKey, List<Passenger> passengers);

        Task<Domain.Reservation> BookRoundTrip(string email, string creditCard, string outboundFlightKey, List<Passenger> outboundPassengers, string inboundFlightKey, List<Passenger> inboundPassengers);
    }
}
