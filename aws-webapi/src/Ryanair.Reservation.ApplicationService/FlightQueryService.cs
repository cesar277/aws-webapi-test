﻿using Ryanair.Reservation.Domain;
using Ryanair.Reservation.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using Ryanair.Reservation.ApplicationService.Interfaces;
using System.Threading.Tasks;

namespace Ryanair.Reservation.ApplicationService {
    public class FlightQueryService : IFlightQueryService {
        private readonly IFlightRepository _flightRepository;
        private readonly IReservationRepository _reservationRepository;

        public FlightQueryService(IFlightRepository flightRepository, IReservationRepository reservationRepository) {
            this._flightRepository = flightRepository;
            this._reservationRepository = reservationRepository;
        }

        public async Task<List<Domain.Flight>> GetAsync(int passengers, string origin, string destination, DateTime dateOut, DateTime dateIn, bool roundTrip) {
            List<Domain.Flight> flights = (await this._flightRepository.GetAsync()).ToList();
            if (dateIn != default(DateTime)) {
                flights = flights.FindAll(f => f.Origin == origin && f.Destination == destination && f.Time > dateIn && f.Time < dateOut);
            } else {
                flights = flights.FindAll(f => f.Origin == origin && f.Destination == destination && f.Time < dateOut);
            }
            if (flights.Count > 0) {
                List<Domain.Reservation> reservations = (await this._reservationRepository.GetAsync()).ToList();
                flights.FindAll(f => {
                    int passengerCount = 0;
                    foreach (var res in reservations) {
                        if (res.InboundFlight.Key == f.Key) {
                            passengerCount += res.PassengersInboundFlight.Count;
                        } else if (res.OutboundFlight.Key == f.Key) {
                            passengerCount += res.PassengersOutboundFlight.Count;
                        }
                    }
                    if (passengerCount == passengers) {
                        return true;
                    }
                    return false;
                });
            }
            return flights;
        }
    }
}
