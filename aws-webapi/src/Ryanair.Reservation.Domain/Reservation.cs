﻿using System.Collections.Generic;

namespace Ryanair.Reservation.Domain {
    public class Reservation {
        public int Id { get; set; }
        public string ReservationNumber { get; set; }
        public string Email { get; set; }
        public string CreditCard { get; set; }
        public Flight InboundFlight { get; set; }
        public Flight OutboundFlight { get; set; }
        public List<Passenger> PassengersInboundFlight { get; set; }
        public List<Passenger> PassengersOutboundFlight { get; set; }
        public bool IsRoundTrip => this.InboundFlight != null;
    }
}
