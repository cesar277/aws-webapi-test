﻿namespace Ryanair.Reservation.Domain {
    public class Passenger {
        public string Name { get; set; }

        public int Bags { get; set; }

        public string Seat { get; set; }
    }
}
